package com.zhuji.markovchain;

import java.io.Serializable;

public class TableItem implements Serializable {
	String fromState;
	String toState;
	int count;
	
	public TableItem(String fromState, String toState, int count) {
		this.fromState = fromState;
		this.toState = toState;
		this.count = count;
	}
	
	/**
	 * for debugging ONLY
	 */
	@Override
	public String toString() {
		return "{"+fromState+"," +toState+","+count+"}";
	}
}