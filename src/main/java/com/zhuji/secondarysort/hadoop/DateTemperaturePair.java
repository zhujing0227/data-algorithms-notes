package com.zhuji.secondarysort.hadoop;

import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * description: DateTemperaturePair
 * 组合键, 先按 yearMonth 排序, 然后按 temperature 排序
 *
 * @author: zhuji
 * @date: created at 2018/10/18 8:58 AM
 */
@Data
@Accessors(chain = true)
public class DateTemperaturePair implements Writable, WritableComparable<DateTemperaturePair> {

    private Text yearMonth = new Text();
    private Text day = new Text();
    private IntWritable temperature = new IntWritable();

    public DateTemperaturePair setYearMonth(String yearMonth) {
        this.yearMonth.set(yearMonth);
        return this;
    }

    public DateTemperaturePair setDay(String day) {
        this.day.set(day);
        return this;
    }

    public DateTemperaturePair setTemperature(int temperature) {
        this.temperature.set(temperature);
        return this;
    }

    /**
     * 控制键的排序
     *
     * @param pair
     * @return
     */
    @Override
    public int compareTo(DateTemperaturePair pair) {
        int compare = this.yearMonth.compareTo(pair.getYearMonth());
        if (compare == 0) {
            compare = temperature.compareTo(pair.getTemperature());
        }
        return -1 * compare;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        yearMonth.write(dataOutput);
        day.write(dataOutput);
        temperature.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        yearMonth.readFields(dataInput);
        day.readFields(dataInput);
        temperature.readFields(dataInput);
    }
}
