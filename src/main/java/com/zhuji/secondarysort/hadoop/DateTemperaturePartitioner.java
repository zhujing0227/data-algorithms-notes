package com.zhuji.secondarysort.hadoop;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * description: DateTemperaturePartitioner
 * 分区控制器, 相同的 yearMonth 分到同一个 reducer, 分区的目的是根据Key值决定Mapper的输出记录被送到哪一个Reducer上去处理
 *
 * @author: zhuji
 * @date: created at 2018/10/18 9:14 AM
 */

public class DateTemperaturePartitioner extends Partitioner<DateTemperaturePair, Text> {

    @Override
    public int getPartition(DateTemperaturePair pair, Text text, int partitions) {
        return Math.abs(pair.getYearMonth().hashCode() % partitions);
    }
}
