package com.zhuji.secondarysort.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * description: SecondarySortDriver
 *
 * @author: zhuji
 * @date: created at 2018/10/18 9:20 AM
 */

public class SecondarySortDriver {

    private static class TemperatureMapper extends Mapper<Object, Text, DateTemperaturePair, IntWritable> {
        @Override
        protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            String[] tokens = value.toString().split(",");
            String yearMonth = tokens[0] + "-" + tokens[1],
                    day = tokens[2];
            Integer temperature = Integer.parseInt(tokens[3]);
            DateTemperaturePair pair = new DateTemperaturePair()
                    .setYearMonth(yearMonth)
                    .setDay(day)
                    .setTemperature(temperature);
            context.write(pair, new IntWritable(temperature));
        }

    }

    private static class TemperatureReducer extends Reducer<DateTemperaturePair, IntWritable, Text, Text> {
        @Override
        protected void reduce(DateTemperaturePair key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            StringBuilder collect = new StringBuilder();
            values.forEach(intWritable -> {
                int i = intWritable.get();
                collect.append(i).append(",");
            });
            context.write(key.getYearMonth(), new Text(collect.toString()));
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        String input = "/Users/zhuji/tem_input";
        String output = "/Users/zhuji/tem_output";

        Job job = Job.getInstance(new Configuration(), SecondarySortDriver.class.getName());

        job.setMapOutputKeyClass(DateTemperaturePair.class);
        job.setMapOutputValueClass(IntWritable.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setMapperClass(TemperatureMapper.class);
        job.setReducerClass(TemperatureReducer.class);
        job.setPartitionerClass(DateTemperaturePartitioner.class);
        job.setGroupingComparatorClass(YearMonthGroupingComparator.class);

        FileInputFormat.setInputPaths(job, new Path(input));
        FileOutputFormat.setOutputPath(job, new Path(output));

        job.waitForCompletion(true);
        System.exit(0);
    }
}
