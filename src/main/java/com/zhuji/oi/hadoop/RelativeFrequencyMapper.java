package com.zhuji.oi.hadoop;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.Arrays;

/**
 * description: RelativeFrequencyMapper
 *
 * @author: zhuji
 * @date: created at 2018/10/26 9:22 AM
 */

public class RelativeFrequencyMapper extends Mapper<Object, Text, PairOfWords, IntWritable> {
    private int neighbor = 2;
    private PairOfWords pair = new PairOfWords();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        neighbor = context.getConfiguration().getInt("neighbor.windown", 2);
    }

    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String[] tokens = value.toString().replaceAll("[,.?\\-()\\d]","").split(" ");
        if (tokens.length < neighbor) {
            return;
        }
        for (int i = 0; i < tokens.length; i++) {
            String word = tokens[i];
            pair.setWord(word);
            int start = i - neighbor < 0 ? 0 : i - neighbor;
            int end = i + neighbor >= tokens.length ? tokens.length - 1 : i + neighbor;
            for (int j = start; j <= end; j++) {
                if (i == j) {
                    continue;
                }
                pair.setNeighbor(tokens[j]);
                context.write(pair, new IntWritable(1));
            }
            pair.setNeighbor("*");
            context.write(pair, new IntWritable(end - start));
        }
    }
}
