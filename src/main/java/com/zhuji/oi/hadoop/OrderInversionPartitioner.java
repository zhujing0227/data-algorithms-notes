package com.zhuji.oi.hadoop;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * description: OrderInversionPartitioner
 *
 * @author: zhuji
 * @date: created at 2018/10/26 9:07 AM
 */

public class OrderInversionPartitioner extends Partitioner<PairOfWords, IntWritable> {

    @Override
    public int getPartition(PairOfWords pairOfWords, IntWritable intWritable, int numPartitions) {
        return Math.toIntExact(Math.abs(hash(pairOfWords.getWord().toString()) % numPartitions));
    }

    private static long hash(String str) {
        long h = 1125899906842597L;
        int l = str.length();
        for (int i = 0; i < l; i++) {
            h = 31 * h + str.charAt(i);
        }
        return h;
    }
}
