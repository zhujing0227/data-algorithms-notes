package com.zhuji.oi.hadoop;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;
import scala.collection.Map;
import scala.collection.immutable.HashMap;

import java.io.IOException;

/**
 * description: RelativeFrequencyReducer
 *
 * @author: zhuji
 * @date: created at 2018/10/26 9:32 AM
 */

public class RelativeFrequencyReducer extends Reducer<PairOfWords, IntWritable, PairOfWords, Double> {
    private double totalCount = 0;
    private String currentWord = "NOT_DEFINED";

    @Override
    protected void reduce(PairOfWords key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        if ("*".equals(key.getNeighbor().toString())) {
            if (key.getWord().toString().equals(currentWord)) {
                totalCount += getTotalCount(values);
            } else {
                currentWord = key.getWord().toString();
                totalCount = getTotalCount(values);
            }
        } else {
            int count = getTotalCount(values);
            context.write(key, count / totalCount);
        }
    }

    private int getTotalCount(Iterable<IntWritable> values) {
        int sum = 0;
        for (IntWritable value : values) {
            sum += value.get();
        }
        return sum;
    }
}
