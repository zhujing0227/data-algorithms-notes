package com.zhuji.oi.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * description: Driver
 * 翻转排序驱动类, 相对频度问题
 *
 * @author: zhuji
 * @date: created at 2018/10/26 9:42 AM
 */

public class Driver {

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        String input = "/Users/zhuji/tem_input";
        String output = "/Users/zhuji/tem_output";

        Job job = Job.getInstance(new Configuration(), Driver.class.getName());
//        job.getConfiguration().set("neighbor.windown", "2");

        job.setMapperClass(RelativeFrequencyMapper.class);
        job.setReducerClass(RelativeFrequencyReducer.class);
        job.setPartitionerClass(OrderInversionPartitioner.class);

        job.setMapOutputKeyClass(PairOfWords.class);
        job.setMapOutputValueClass(IntWritable.class);
        job.setOutputKeyClass(PairOfWords.class);
        job.setOutputValueClass(Double.class);

        FileInputFormat.setInputPaths(job, new Path(input));
        FileOutputFormat.setOutputPath(job, new Path(output));

        job.waitForCompletion(true);
        System.exit(0);

    }

}
