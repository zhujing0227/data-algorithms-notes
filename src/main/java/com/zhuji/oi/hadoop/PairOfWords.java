package com.zhuji.oi.hadoop;

import com.sun.corba.se.spi.ior.Writeable;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;

/**
 * description: PairOfWords
 *
 * @author: zhuji
 * @date: created at 2018/10/26 9:07 AM
 */
@Data
@Accessors(chain = true)
public class PairOfWords implements WritableComparable<PairOfWords>, Serializable {

    private Text word = new Text();
    private Text neighbor = new Text();

    public PairOfWords setWord(String word) {
        this.word.set(word);
        return this;
    }

    public PairOfWords setNeighbor(String neighbor) {
        this.neighbor.set(neighbor);
        return this;
    }

    @Override
    public String toString() {
        return "(" + word + "," + neighbor + ")";
    }

    @Override
    public int compareTo(PairOfWords o) {
        return this.word.compareTo(o.word);
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        word.write(dataOutput);
        neighbor.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        word.readFields(dataInput);
        neighbor.readFields(dataInput);
    }
}
