package com.zhuji.topn.hadoop;

import com.zhuji.secondarysort.hadoop.SecondarySortDriver;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * description: TopNDriver
 *
 * @author: zhuji
 * @date: created at 2018/10/23 9:41 AM
 */

public class TopNDriver {

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        String input = "/Users/zhuji/tem_input";
        String output = "/Users/zhuji/tem_output";

        Job job = Job.getInstance(new Configuration(), SecondarySortDriver.class.getName());
        job.getConfiguration().setInt("top.n", 5);

        job.setMapperClass(TopNMapper.class);
        job.setReducerClass(TopNReducer.class);

        job.setMapOutputKeyClass(NullWritable.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(DoubleWritable.class);
        job.setOutputValueClass(Text.class);

        FileInputFormat.setInputPaths(job, new Path(input));
        FileOutputFormat.setOutputPath(job, new Path(output));

        job.waitForCompletion(true);
        System.exit(0);
    }

}
