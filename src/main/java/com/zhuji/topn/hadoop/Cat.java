package com.zhuji.topn.hadoop;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * description: Cat
 *
 * @author: zhuji
 * @date: created at 2018/10/23 9:11 AM
 */

@Data
@Accessors(chain = true)
public class Cat implements Serializable {
    private String catId;
    private String catName;
    private Double catWeight;

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
