package com.zhuji.topn.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * description: TopNMapper
 *
 * @author: zhuji
 * @date: created at 2018/10/23 9:16 AM
 */

public class TopNMapper extends Mapper<Object, Text, NullWritable, Text> {
    private int defaultN = 10;
    private SortedMap<Double, Cat> top = new TreeMap<>();

    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String[] tokens = value.toString().split(",");
        Double weight = Double.parseDouble(tokens[0]);
        String catId = tokens[1], catName = tokens[2];
        top.put(weight, new Cat().setCatId(catId).setCatName(catName).setCatWeight(weight));
        if (top.size() > defaultN) {
            top.remove(top.lastKey());//bottomN
//            top.remove(top.firstKey());//topN
        }

        top.forEach((w, cat) -> {
            try {
                context.write(NullWritable.get(), new Text(cat.toString()));
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        Configuration configuration = context.getConfiguration();
        defaultN = configuration.getInt("top.n", defaultN);
    }
}
