package com.zhuji.topn.hadoop;

import com.alibaba.fastjson.JSONObject;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * description: TopNReducer
 *
 * @author: zhuji
 * @date: created at 2018/10/23 9:33 AM
 */

public class TopNReducer extends Reducer<NullWritable, Text, DoubleWritable, Text> {
    private int defaultN = 10;
    private SortedMap<Double, Cat> top = new TreeMap<>();

    @Override
    protected void reduce(NullWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        values.forEach(text -> {
            Cat cat = JSONObject.parseObject(text.toString(), Cat.class);
            top.put(cat.getCatWeight(), cat);
            if (top.size() > defaultN) {
                top.remove(top.lastKey());//bottomN
//            top.remove(top.firstKey());//topN
            }
        });

        top.forEach((w, cat) -> {
            try {
                context.write(new DoubleWritable(w), new Text(cat.toString()));
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        Configuration configuration = context.getConfiguration();
        defaultN = configuration.getInt("top.n", defaultN);
    }

}