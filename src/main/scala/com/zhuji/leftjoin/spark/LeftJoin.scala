package com.zhuji.leftjoin.spark

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
  * description: LeftJoin
  * map-reduce方式左连接
  */

object LeftJoin extends App {
  val sc = new SparkContext("local", "takeOrdered")

  //userId->("L",location)
  val users = sc.textFile("/Users/zhuji/users")
    .map(line => line.split(","))
    .map(u => (u(0), ("L", u(1))))
    .persist()

  //userId->("P",product)
  val transaction = sc.textFile("/Users/zhuji/trans")
    .map(line => line.split(","))
    .map(t => (t(2), ("P", t(1))))
    .persist()

  //userId->Iterable[("L",location1),("P",product1)...]
  val groupedRDD = users.union(transaction)
    .groupByKey()
    .persist()
  println(groupedRDD.toDebugString)

  //product->List[location]
  val result = groupedRDD.flatMapValues(iter => {
    val stringToTuples = iter.groupBy(f => f._1)
    val locations = stringToTuples.getOrElse("L", Iterable.empty)
    val products = stringToTuples.getOrElse("P", Iterable.empty)
    products.map(_._2).map(p => locations.map(_._2).map(l => (p, l))).toList
  })
    .reduce((a, b) => ("", a._2 ++ b._2))
    ._2
    .groupBy(_._1)
    .mapValues(iter => iter.map(_._2).toList)

  println(result.mkString)
}


/**
  * Spark 的 leftouterjoin()
  */
object LeftOuterJoin extends App {
  val sc = new SparkContext("local", "takeOrdered")

  //userId->("L",location)
  val users = sc.textFile("/Users/zhuji/users")
    .map(line => line.split(","))
    .map(u => (u(0), u(1)))
    .persist()

  //userId->("P",product)
  val transaction = sc.textFile("/Users/zhuji/trans")
    .map(line => line.split(","))
    .map(t => (t(2), t(1)))
    .persist()

  //(user,(product,Option(location))
  private val joined: RDD[(String, (String, Option[String]))] = transaction.leftOuterJoin(users)

  //(product,location)
  private val products: RDD[(String, String)] = joined.flatMapValues(v => v._2.map(location => (v._1, location)))
    .map(_._2)

  //(product,(Set[location],count))
  private val value: RDD[(String, (Set[String], Int))] = products.groupByKey()
    .mapValues(iter => (iter.toSet, iter.toSet.size))


  value.saveAsTextFile("/Users/zhuji/scalaout")
  /*
    (p1,(Set(GA, UT),2))
    (p2,(Set(GA),1))
    (p3,(Set(UT),1))
    (p4,(Set(CA, GA, UT),3))
   */
}