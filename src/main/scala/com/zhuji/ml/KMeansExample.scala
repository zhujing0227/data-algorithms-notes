package com.zhuji.ml

import org.apache.spark.SparkContext
import org.apache.spark.mllib.clustering.{KMeans, KMeansModel}
import org.apache.spark.mllib.linalg
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

/**
  * description: KMeans
  *
  */

object KMeansExample extends App {
  val sc = new SparkContext("local", "RecommendFriend")
  sc.setLogLevel("INFO")
  val sparkSession = SparkSession.builder().getOrCreate()

  private val parsedData: RDD[linalg.Vector] = sc.textFile("/Users/zhuji/kmeans_data")
    .map(l => Vectors.dense(l.split(" ").map(_.toDouble))).cache()

  private val kMeansModel: KMeansModel = KMeans.train(parsedData, 2, 20)
  private val cost: Double = kMeansModel.computeCost(parsedData)
  println(s"Within Set Sum of Squared Errors = $cost")

  kMeansModel.save(sc, "/Users/zhuji/ml/KMeansModel")

  private val model: KMeansModel = KMeansModel.load(sc, "target/com/zhuji/ml/KMeansModel")
  println(model)

  sc.stop()
}

/*
0.0 0.0 0.0
0.1 0.1 0.1
0.2 0.2 0.2
9.0 9.0 9.0
9.1 9.1 9.1
9.2 9.2 9.2
 */