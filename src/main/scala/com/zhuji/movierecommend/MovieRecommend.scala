package com.zhuji.movierecommend

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

/**
  * description: MovieRecommend
  * 基于内容的电影推荐
  */

object MovieRecommend {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local", "RecommendFriend")
    val sparkSession = SparkSession.builder().getOrCreate()

    import sparkSession.implicits._

    //(userId,movieId,rating)
    val originalRdd: RDD[(String, String, Int)] = sc.textFile("/Users/zhuji/tem_input")
      .map(line => line.split(" "))
      .map(a => (a(0), a(1), a(2).toInt))
      .cache()

    //(movieId,(rating,count)), 每部电影对应分数的总评分人数
    val movieIdToRatingToCount: collection.Map[String, Map[Int, Int]] =
      originalRdd.mapPartitions(iter => iter.map(one => (one._2, one._3)))
        .aggregateByKey(Map[Int, Int]())((m, rating) => m.updated(rating, m.getOrElse(rating, 0) + 1), _ ++ _)
        .collectAsMap()

    val usersRdd: RDD[(String, (String, Int, Int))] =
      originalRdd.mapPartitions(iter => iter.map(one => (one._1, (one._2, one._3, movieIdToRatingToCount.get(one._2).flatMap(m => m.get(one._3))))))
        .flatMapValues(o => o._3.map(count => (o._1, o._2, count)))
        .cache()

    usersRdd.toDF("user", "rating").show(1000000, truncate = false)
    /*
    +-----+--------------+
    |user |rating        |
    +-----+--------------+
    |user1|[movie1, 3, 1]|
    |user1|[movie2, 4, 1]|
    |user1|[movie3, 2, 2]|
    |user2|[movie1, 2, 2]|
    |user2|[movie2, 5, 1]|
    |user2|[movie3, 3, 2]|
    |user2|[movie4, 5, 1]|
    |user3|[movie1, 2, 2]|
    |user3|[movie2, 3, 2]|
    |user3|[movie3, 2, 2]|
    |user4|[movie1, 5, 1]|
    |user4|[movie2, 3, 2]|
    |user4|[movie3, 3, 2]|
    |user4|[movie4, 2, 1]|
    |user4|[movie5, 3, 1]|
    +-----+--------------+
     */

    val filterRdd: RDD[(String, ((String, Int, Int), (String, Int, Int)))] = usersRdd.leftOuterJoin(usersRdd)
      .flatMap(t => t._2._2.map(m2 => (t._1, (t._2._1, m2))))
      .filter(t => t._2._1._1 < t._2._2._1) //去重
      .cache()

    filterRdd.toDF("user", "ratingTuple").show(10000, truncate = false)
    /*
    +-----+--------------------------------+
    |user |ratingTuple                     |
    +-----+--------------------------------+
    |user2|[[movie1, 2, 2], [movie2, 5, 1]]|
    |user2|[[movie1, 2, 2], [movie3, 3, 2]]|
    |user2|[[movie1, 2, 2], [movie4, 5, 1]]|
    |user2|[[movie2, 5, 1], [movie3, 3, 2]]|
    |user2|[[movie2, 5, 1], [movie4, 5, 1]]|
    |user2|[[movie3, 3, 2], [movie4, 5, 1]]|
    |user3|[[movie1, 2, 2], [movie2, 3, 2]]|
    |user3|[[movie1, 2, 2], [movie3, 2, 2]]|
    |user3|[[movie2, 3, 2], [movie3, 2, 2]]|
    |user1|[[movie1, 3, 1], [movie2, 4, 1]]|
    |user1|[[movie1, 3, 1], [movie3, 2, 2]]|
    |user1|[[movie2, 4, 1], [movie3, 2, 2]]|
    |user4|[[movie1, 5, 1], [movie2, 3, 2]]|
    |user4|[[movie1, 5, 1], [movie3, 3, 2]]|
    |user4|[[movie1, 5, 1], [movie4, 2, 1]]|
    |user4|[[movie1, 5, 1], [movie5, 3, 1]]|
    |user4|[[movie2, 3, 2], [movie3, 3, 2]]|
    |user4|[[movie2, 3, 2], [movie4, 2, 1]]|
    |user4|[[movie2, 3, 2], [movie5, 3, 1]]|
    |user4|[[movie3, 3, 2], [movie4, 2, 1]]|
    |user4|[[movie3, 3, 2], [movie5, 3, 1]]|
    |user4|[[movie4, 2, 1], [movie5, 3, 1]]|
    +-----+--------------------------------+
     */

    //((movie1,movie2),(pearson,cosine,jaccard))
    val similarity: RDD[((String, String), (Double, Double, Double))] = filterRdd.mapPartitions(iter => iter.map(t => {
      val movie1 = t._2._1
      val movie2 = t._2._2

      val ratingProduct = movie1._2 * movie2._2
      val rating1Squared = movie1._2 * movie1._2
      val rating2Squared = movie2._2 * movie2._2
      //(movie1,movie2)(rating1,numbers1,rating2,numbers2,product,squared1,squared2)
      ((movie1._1, movie2._1), (movie1._2, movie1._3, movie2._2, movie2._3, ratingProduct, rating1Squared, rating2Squared))
    }))
      .groupByKey()
      .mapValues(t7s => {
        val (rating1Sum, raters1, rating2Sum, raters2, dotProduct, rating1Sq, rating2Sq) =
          t7s.reduceLeft((a1, a2) => (a1._1 + a2._1, a1._2.max(a2._2), a1._3 + a2._3, a1._4.max(a2._4), a1._5 + a2._5, a1._6 + a2._6, a1._7 + a2._7))
        val pearson = calculatePearsonCorrelation(t7s.size, dotProduct, rating1Sum, rating2Sum, rating1Sq, rating2Sq)
        val cosine = calculateCosineCorrelation(dotProduct, Math.sqrt(rating1Sq), Math.sqrt(rating2Sq))
        val jaccard = calculateJaccardCorrelation(t7s.size, raters1, raters2)
        (pearson, cosine, jaccard)
      })
      .sortByKey()
      .cache()

    similarity.toDF("movie1-movie2", "pearson-cosine-jaccard").show(10000, truncate = false)
    /*
    +----------------+---------------------------------------------------+
    |movie1-movie2   |pearson-cosine-jaccard                             |
    +----------------+---------------------------------------------------+
    |[movie1, movie2]|[-0.492365963917331, 0.8638091589670809, Infinity] |
    |[movie1, movie3]|[0.4082482904638631, 0.9381026756366438, Infinity] |
    |[movie1, movie4]|[-1.0, 0.6896551724137931, 2.0]                    |
    |[movie1, movie5]|[NaN, 1.0, 1.0]                                    |
    |[movie2, movie3]|[0.30151134457776363, 0.9702215502575142, Infinity]|
    |[movie2, movie4]|[1.0, 0.9872411207126471, 2.0]                     |
    |[movie2, movie5]|[NaN, 1.0, 0.5]                                    |
    |[movie3, movie4]|[NaN, 0.919145030018058, 2.0]                      |
    |[movie3, movie5]|[NaN, 1.0, 0.5]                                    |
    |[movie4, movie5]|[NaN, 1.0, 1.0]                                    |
    +----------------+---------------------------------------------------+
     */

  }


  /** 皮尔逊相似度
    * (n*dotProduct(A,B)-∑A*∑B)/(sqrt(n*∑A*A-∑A*∑A)*sqrt(n*∑B*B-∑B*∑B))
    */
  def calculatePearsonCorrelation(size: Double, dotProduct: Double,
                                  rating1Sum: Double, rating2Sum: Double,
                                  rating1NormSq: Double, rating2NormSq: Double): Double = {
    val numerator = size * dotProduct - rating1Sum * rating2Sum
    val denominator = Math.sqrt(size * rating1NormSq - rating1Sum * rating1Sum) * Math.sqrt(size * rating2NormSq - rating2Sum * rating2Sum)
    numerator / denominator
  }

  /** 余弦相似度
    * The cosine similarity between two vectors A, B is
    * dotProduct(A, B) / (norm(A) * norm(B))
    */
  def calculateCosineCorrelation(dotProduct: Double,
                                 rating1Norm: Double, rating2Norm: Double): Double = dotProduct / (rating1Norm * rating2Norm)

  /** 杰卡德相似度
    * The Jaccard Similarity between two sets A, B is
    * |Intersection(A, B)| / |Union(A, B)|
    */
  def calculateJaccardCorrelation(inCommon: Double, totalA: Double, totalB: Double): Double = {
    val union = totalA + totalB - inCommon
    inCommon / union
  }
}

/*
#input
user1 movie1 3
user1 movie2 4
user1 movie3 2
user2 movie1 2
user2 movie2 5
user2 movie3 3
user2 movie4 5
user3 movie1 2
user3 movie2 3
user3 movie3 2
user4 movie1 5
user4 movie2 3
user4 movie3 3
user4 movie4 2
user4 movie5 3
 */