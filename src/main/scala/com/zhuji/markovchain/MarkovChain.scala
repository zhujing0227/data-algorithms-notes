package com.zhuji.markovchain

import java.time.format.DateTimeFormatter
import java.time.{LocalDate, ZoneOffset}

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

/**
  * description: MarkovChain
  *
  */

object MarkovChain {
  val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local", "RecommendFriend")
    sc.setLogLevel("INFO")

    val sparkSession = SparkSession.builder().getOrCreate()

    import sparkSession.implicits._
    val stateSequence: RDD[(String, List[String])] = sc.textFile("/Users/zhuji/markovchain")
      .mapPartitions(iter => iter.map(l => l.split(",")))
      .map(a => (a(0), (dateStr2Milliseconds(a(2), formatter), a(3).toInt)))
      .aggregateByKey(List[(Long, Int)]())((s, d) => d :: s, _ ++ _)
      .mapValues(toStateSequence)
      .cache()
    stateSequence.toDF().show(100, truncate = false)
    /*
    +----------+----------------------------------------------------------------------------------------+
    |_1        |_2                                                                                      |
    +----------+----------------------------------------------------------------------------------------+
    |0IROUCA5O2|[SG, SL, SG, SL, SG, ML, SG, SL, SG, SL]                                                |
    |4N0B1U5HVG|[SG, ML, MG, SG, SL, SG, SL, SG, ML]                                                    |
    |3KJR1907D9|[SG, ME, SL, ME, MG, SG]                                                                |
    |8555DQOK14|[SG, ML, LL]                                                                            |
    |J6VXOTY7IA|[SG, ML, SG, SL, SG, ML, SG]                                                            |
    |8VY8W2UD8W|[SG, SL, ML, LL, SG]                                                                    |
    |1BNFI5D3Z1|[SG, SL, SG, SL, SG, SL, SG, SL]                                                        |
    |J0BO64O93C|[SG, SL, SG, SL, ML, MG, SG, SL, SG, SL, SG, SL, ML, SG, SL]                            |
    |NT58RT7KK4|[MG, SG, SL, SG, SL, SG, SL, SG, SL, SG]                                                |
    |HBD6YAC69Y|[SG, SL, SG, SL, SG, SE, SL, SE, ML, MG]                                                |
    |8ZT0I04GOV|[LL, SG, SL, SG]                                                                        |
    |T29M0VFTO4|[SG, SL, SG, SL, ML, SG, SL, SG, SL, SG, SL, SG, SL, SG]                                |
    |I4BMN290GV|[SG, SL, SG, SL, SE, SG, LL, SG]                                                        |
    |X1JZ3R34CR|[SG, SL, SG, SL, SG, SL, ML, SG, SL]                                                    |
    |TO83S4RI1S|[SG, SL, SG, ML, SG, LL, SG]                                                            |
    |6DCNO2QSB7|[SG, SL, SG, SL]                                                                        |
    |O60F4O7BJS|[LL, SG, SL, ML]                                                                        |
    |4O00K7LEA4|[SG, SL, SG, SL, SG, SL, SG, SL, SG, SL, SG, SL, SG, SL]                                |
    |WUJJZBYZIF|[SG, SL, SG, ML, SG, SL, SG, SL, SG]                                                    |
    |48I9334A32|[SG, SL, SG, SL, ML, MG, SG, SL, SG]                                                    |
    |FIJHE3BB74|[SG, SL, ML, SG, LL, SG]                                                                |
    |G04LO10S2H|[SG, SL, SG, SL, SG, SL, SG, MG, SL, SG, SG, SL, SG, SL, SG]                            |
     */

    //状态转移矩阵((from,to),count)
    val markovModel: RDD[((String, String), Int)] = stateSequence.mapValues(seq => {
      (for ((f, i) <- seq.view(0, seq.size - 1).zipWithIndex)
        yield ((f, seq(i + 1)), 1)).toList
    })
      .flatMap(a => a._2)
      .reduceByKey(_ + _)
      .cache()
    markovModel.toDF().show(100, truncate = false)
    /*
    +--------+------+
    |_1      |_2    |
    +--------+------+
    |[SL, LL]|7670  |
    |[LL, SL]|70    |
    |[SL, MG]|1279  |
    |[MG, LL]|803   |
    |[LE, LG]|2     |
    |[SL, ME]|163   |
    |[LG, LL]|64    |
    |[ML, LG]|59    |
    |[SE, LE]|1     |
    |[LE, SG]|59    |
    |[SE, SL]|4951  |
    |[SG, ME]|615   |
    |[SL, SE]|3813  |
    |[ME, SL]|620   |
    |[ME, SG]|181   |
    |[SL, LG]|233   |
    |[SE, SG]|3442  |
    |[MG, SG]|19364 |
    |[LL, MG]|2990  |
    |[SL, SL]|4595  |
    |[LG, LE]|1     |
    |[SG, SG]|11301 |
     */

    //生成markov 概率模型
    val map: Map[Int, String] = Map(0 -> "SL", 5 -> "MG", 1 -> "SE", 6 -> "LL", 2 -> "SG", 7 -> "LE", 3 -> "ML", 8 -> "LG", 4 -> "ME")
    val tableItems = markovModel.mapPartitions(iter => iter.map(a => new TableItem(a._1._1, a._1._2, a._2)))
      .collect()
      .toList

    import StateTransitionTableBuilder._

    import scala.collection.JavaConversions._
    val doubleses = generateStateTransitionTable(tableItems)
      .map(a => a.map(a => (a * 100.0).formatted("%.3g").toDouble))
    println(map.toList.sortBy(_._1).map(_._2).mkString("\t", "\t\t\t", ""))
    for ((elem, index) <- doubleses.zipWithIndex) {
      println(elem.mkString(s"${map(index)}   ", "\t\t", "\n"))
    }
    /*
          SL		  SE			SG			ML			ME			MG			LL			   LE			LG
    SL   2.07		1.72		76.5		15.4		0.0734		0.576		3.45		0.0113		0.105

    SE   53.2		0.419		37.0		7.77		0.0215		0.0752	1.48		0.0107		0.0215

    SG   74.9		1.85		3.63		15.7		0.197		  0.107		3.57		0.0456		0.0218

    ML   0.342	0.00126	80.8		0.00126	0.00126		15.7		3.12		0.00126		0.0759

    ME   73.3		0.118		21.5		0.118		0.118		  3.78		0.708		0.118		  0.236

    MG   7.88		0.461		74.4		13.4		0.00384		0.665		3.09		0.00384		0.073

    LL   0.344	0.00484	82.7		0.00484	0.00484		14.5		0.00484	0.00969		2.46

    LE   64.6		0.448		26.9		0.448		0.448		  4.93		0.448		0.448		  1.35

    LG   20.5		0.0413	62.9		0.0413	0.0413		13.6		2.69		0.0827		0.0827
     */
  }

  /**
    * 将有序表转换为状态序列
    */
  def toStateSequence(seq: List[(Long, Int)]): List[String] = {
    val tuples = seq.sortBy(_._1)
    (for ((f, i) <- tuples.view(0, tuples.size - 1).zipWithIndex)
      yield getElapsedTime(f._1, tuples(i + 1)._1) + getAmountRange(f._2, tuples(i + 1)._2)).toList
  }

  def getElapsedTime(priorTime: Long, time: Long): String = {
    val daysDiff = (time - priorTime) / 86400000
    if (daysDiff < 30) "S" else if (daysDiff < 60) "M" else "L"
  }

  def getAmountRange(priorAmount: Int, amount: Int): String =
    if (priorAmount < 0.9 * amount) "L" else if (priorAmount < 1.1 * amount) "E" else "G"

  def dateStr2Milliseconds(dateStr: String, formatter: DateTimeFormatter): Long =
    LocalDate.parse(dateStr, formatter).atTime(0, 0, 0).toInstant(ZoneOffset.of("+8")).toEpochMilli
}
