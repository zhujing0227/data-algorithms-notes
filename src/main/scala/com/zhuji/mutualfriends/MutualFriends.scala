package com.zhuji.mutualfriends

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

/**
  * description: MutualFriends
  * 共同好友
  */

object MutualFriends {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local", "MovingAverage")
    val sparkSession = SparkSession.builder().getOrCreate()

    //((person,friend),friends)
    val friends: RDD[((String, String), Array[String])] = sc.textFile("/Users/zhuji/tem_input")
      .map(l => {
        val a = l.split(",")
        (a(0), a(1).split(" "))
      })
      .map(a => a._2.map(f => if (a._1 < f) ((a._1, f), a._2) else ((f, a._1), a._2))) //避免出现(a,b)、(b,a)相同的 key
      .flatMap(a => a)
      .cache()

    import sparkSession.implicits._
    friends.toDF("person-friend", "friedns")
      .show(false)
    /*
    +-------------+-------------------------+
    |person-friend|friedns                  |
    +-------------+-------------------------+
    |[100, 200]   |[200, 300, 400, 500, 600]|
    |[100, 300]   |[200, 300, 400, 500, 600]|
    |[100, 400]   |[200, 300, 400, 500, 600]|
    |[100, 500]   |[200, 300, 400, 500, 600]|
    |[100, 600]   |[200, 300, 400, 500, 600]|
    |[100, 200]   |[100, 300, 400]          |
    |[200, 300]   |[100, 300, 400]          |
    |[200, 400]   |[100, 300, 400]          |
    |[100, 300]   |[100, 200, 400, 500]     |
    |[200, 300]   |[100, 200, 400, 500]     |
    |[300, 400]   |[100, 200, 400, 500]     |
    |[300, 500]   |[100, 200, 400, 500]     |
    |[100, 400]   |[100, 200, 300]          |
    |[200, 400]   |[100, 200, 300]          |
    |[300, 400]   |[100, 200, 300]          |
    |[100, 500]   |[100, 300]               |
    |[300, 500]   |[100, 300]               |
    |[100, 600]   |[100]                    |
    +-------------+-------------------------+
     */

    //((person,friend),mutualFriends)
    val mutualFriends: RDD[((String, String), Set[String])] = friends.aggregateByKey(Set[String]())((s, a) => if (s.isEmpty) s ++ a else s intersect a.toSet, _ intersect _)
      .sortByKey()
      .cache()
    mutualFriends.toDF("person-friend", "mutualFriends")
      .show(false)
    /*
    +-------------+---------------+
    |person-friend|mutualFriends  |
    +-------------+---------------+
    |[100, 200]   |[400, 300]     |
    |[100, 300]   |[400, 500, 200]|
    |[100, 400]   |[300, 200]     |
    |[100, 500]   |[300]          |
    |[100, 600]   |[]             |
    |[200, 300]   |[100, 400]     |
    |[200, 400]   |[100, 300]     |
    |[300, 400]   |[100, 200]     |
    |[300, 500]   |[100]          |
    +-------------+---------------+
     */

  }
}
