package com.zhuji.movingaverage.spark

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

/**
  * description: MovingAverage
  * 移动平均值, 先按公司分组, 再按时间排序
  * 未解决中间缺数据的情况
  */

object MovingAverage {

  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local", "MovingAverage")
    val sparkSession = SparkSession.builder().getOrCreate()
    val windownBroadcast = sc.broadcast(3)

    import sparkSession.implicits._
    //(key,(date,price))
    val rdd: RDD[(String, (String, Double))] = sc.textFile("/Users/zhuji/tem_input")
      .mapPartitions(iter => iter.map(str => str.split(",")))
      .map(arr => (arr(0), (arr(1), arr(2).toDouble)))
      .sortBy(_._2)
      .cache()

    rdd.toDF("company", "date-price")
      .show(100, truncate = false)
    /*
    +-------+--------------------+
    |company|date-price          |
    +-------+--------------------+
    |GOOG   |[2004-07-17, 918.55]|
    |GOOG   |[2004-07-18, 910.68]|
    |GOOG   |[2004-07-19, 896.6] |
    |GOOG   |[2004-11-02, 194.87]|
    |GOOG   |[2004-11-03, 191.67]|
    |GOOG   |[2004-11-04, 184.7] |
    |IBM    |[2013-09-25, 189.47]|
    |IBM    |[2013-09-26, 190.22]|
    |IBM    |[2013-09-27, 186.92]|
    |IBM    |[2013-09-30, 185.18]|
    |AAPL   |[2013-10-03, 483.41]|
    |AAPL   |[2013-10-04, 483.03]|
    |AAPL   |[2013-10-07, 487.75]|
    |AAPL   |[2013-10-08, 480.94]|
    |AAPL   |[2013-10-09, 486.59]|
    +-------+--------------------+
    */

    //(key,(date,average))
    val result: RDD[(String, (String, Double))] = rdd.groupByKey()
      .mapValues(s => {
        val tuples = s.toArray.sortWith((a1, a2) => a1._1.compareTo(a2._1) < 0)
        println(tuples.mkString)
        for ((t, index) <- tuples.zipWithIndex)
          yield {
            val tem: Array[Double] = tuples.drop(if (index >= windownBroadcast.value) 0 else index - windownBroadcast.value)
              .dropRight(tuples.length - index - 1)
              .map(_._2)

            (t._1, tem.sum / tem.length)
          }
      })
      .flatMapValues(a => a)
      .cache()
    result.toDF("company", "date-average")
      .show(100, truncate = false)
    /*
    +-------+--------------------------------+
    | company | date - average |
    +-------+--------------------------------+
    | IBM |[2013 - 0 9 - 25, 189.47] |
    | IBM |[2013 - 0 9 - 26, 189.845] |
    | IBM |[2013 - 0 9 - 27, 188.87] |
    | IBM |[2013 - 0 9 - 30, 187.9475] |
    | GOOG |[2004 - 07 - 17, 918.55] |
    | GOOG |[2004 - 07 - 18, 914.615] |
    | GOOG |[2004 - 07 - 19, 908.61] |
    | GOOG |[2004 - 11 - 02, 730.175] |
    | GOOG |[2004 - 11 - 03, 622.4739999999999] |
    | GOOG |[2004 - 11 - 04, 549.5116666666667] |
    | AAPL |[2013 - 10 - 03, 483.41] |
    | AAPL |[2013 - 10 - 04, 483.22] |
    | AAPL |[2013 - 10 - 07, 484.73] |
    | AAPL |[2013 - 10 - 0 8, 483.7825] |
    | AAPL |[2013 - 10 - 0 9, 484.34400000000005] |
    +-------+--------------------------------+
    */
  }
}