package com.zhuji.topn.spark

import org.apache.spark.SparkContext

/**
  * description: Top10NonUnique
  */

object Top10NonUnique extends App {
  val topN = 3
  val sc = new SparkContext("local", "takeOrdered")
  val broadcast = sc.broadcast(topN)

  implicit val ordering: Ordering[(String, Int)] = Ordering.fromLessThan((t1, t2) => t1._2 > t2._2)

  val sortRdd = sc.textFile("/Users/zhuji/tem_input")
    .coalesce(9)
    .map(line => line.split(","))
    .map(arr => {
      val url = arr(0)
      val count = arr(1).toInt
      (url, count)
    })
    .reduceByKey(_ + _)
    .sortBy(f => f._2, ascending = false) //top
    //    .sortBy(f => f._2, ascending = true)  //bottom
    .persist()

  sortRdd.saveAsTextFile("/Users/zhuji/scalaout")

  //  private val string: String = sortRdd.top(broadcast.value).mkString
  private val string: String = sortRdd.takeOrdered(broadcast.value).mkString
  println(string)
}

