package com.zhuji.topn.spark

import com.zhuji.topn.hadoop.Cat
import org.apache.spark.SparkContext

import scala.collection.immutable.TreeMap

/**
  * 所有的 key 是唯一的
  * description: TopN
  */

object TopNUnique {
  def main(args: Array[String]): Unit = {
    val topN = 5
    val sc = new SparkContext("local", "topN")
    val broadcastTopN = sc.broadcast(topN)

    val sortRdd = sc.textFile("/Users/zhuji/tem_input")
      .map(line => line.split(","))
      .map(arr => {
        val weight = arr(0).toDouble
        val catId = arr(1)
        val catName = arr(2)
        (weight, new Cat().setCatId(catId).setCatName(catName).setCatWeight(weight))
      })
      .mapPartitions(iter => {
        var top10 = new TreeMap[Double, Cat]()
        for (elem <- iter) {
          top10 = top10 + elem
          if (top10.size > broadcastTopN.value) {
            top10 = top10 - top10.lastKey //bottom
            //top10 = top10 - top10.firstKey  //top
          }
        }
        top10.iterator
      })
      .sortByKey(ascending = false)
      .persist()

    sortRdd.saveAsTextFile("/Users/zhuji/scalaout")

    sc.stop()
  }
}
