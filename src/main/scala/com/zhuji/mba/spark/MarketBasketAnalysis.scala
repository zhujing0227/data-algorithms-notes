package com.zhuji.mba.spark

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

/**
  * description: MarketBasketAnalysis
  * 购物篮分析, 支持度、置信度、提升度
  */

object MarketBasketAnalysis {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local", "MovingAverage")
    val sparkSession = SparkSession.builder().getOrCreate()

    val rdd: RDD[Array[String]] = sc.textFile("/Users/zhuji/tem_input")
      .map(l => l.split(":")(1).split(",").sorted)
      .cache()
    val count = sc.broadcast(rdd.count())

    //((a,b),frequency)
    //支持度 support(A=>B) = P(A U B)
    //定义：设W 中有s ％的事务同时支持物品集A 和B，s ％称为关联规则A→B 的支持度。支持度描述了A 和B 这两个物品集的并集C 在所有的事务中出现的概率有多大。
    val support: RDD[((String, String), Double)] = rdd.map(sorted => {
      for ((first, index) <- sorted.zipWithIndex;
           second <- sorted.drop(index + 1))
        yield ((first, second), 1)
    })
      .flatMap(a => a)
      .aggregateByKey(0)((a, b) => a + b, _ + _)
      .mapValues(f => f.toDouble / count.value)
      .cache()

    import sparkSession.implicits._
    support.toDF("LHS-RHS", "frequency")
      .show()
    /*
    +-------+---------+
    |LHS-RHS|frequency|
    +-------+---------+
    | [a, b]|      0.5|
    | [a, d]|     0.25|
    | [a, c]|     0.25|
    | [b, d]|     0.25|
    | [b, c]|     0.75|
    +-------+---------+
     */

    //(hs,frequency)
    val lhsFrequency = rdd.map(sorted => sorted.map(s => (s, 1)))
      .flatMap(a => a)
      .aggregateByKey(0)((a, b) => a + b, _ + _)
      .mapValues(f => f.toDouble / count.value)
      .collectAsMap()
    println(lhsFrequency) //Map(b -> 1.0, d -> 0.25, a -> 0.5, c -> 0.75)

    //((a,b),confidence)
    //置信度 confidence(A=>B) = P(A U B)/P(A)
    //定义: 设W中支持物品集A的事务中，有c ％的事务同时也支持物品集B，c ％称为关联规则A→B 的可信度
    val confidence: RDD[((String, String), Double)] = support.map(a => (a._1, a._2 / lhsFrequency.getOrElse(a._1._1, 1.0)))

    confidence.toDF("LHS-RHS", "confidence")
      .show()
    /*
    +-------+----------+
    |LHS-RHS|confidence|
    +-------+----------+
    | [a, b]|       1.0|
    | [a, d]|       0.5|
    | [a, c]|       0.5|
    | [b, d]|      0.25|
    | [b, c]|      0.75|
    +-------+----------+
     */

    //提升度 lift(A=>B) = P(A U B)/P(A)/P(B) = confidence(A=>B)/P(B)
    //定义: 表示“包含A的事务中同时包含B事务的比例”与“包含B事务的比例”的比值, 提升度反映了关联规则中的A与B的相关性，提升度>1且越高表明正相关性越高，提升度<1且越低表明负相关性越高，提升度=1表明没有相关性。
    val lift: RDD[((String, String), Double)] = confidence.map(a => (a._1, a._2 / lhsFrequency.getOrElse(a._1._2, 1.0)))

    lift.toDF("LHS-RHS", "lift")
      .show()
    /*
    +-------+------------------+
    |LHS-RHS|              lift|
    +-------+------------------+
    | [a, b]|               1.0|
    | [a, d]|               2.0|
    | [a, c]|0.6666666666666666|
    | [b, d]|               1.0|
    | [b, c]|               1.0|
    +-------+------------------+
     */
  }
}
