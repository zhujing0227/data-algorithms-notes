package com.zhuji.oi.spark

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

/**
  * description: OrderInversion
  * 翻转排序, 相对频度问题
  */

object OrderInversion {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local", "secondarysort")
    val sparkSession = SparkSession.builder().getOrCreate()
    val neighborBroadcast = sc.broadcast(2)

    //(word,neighbor)
    val neighbors: RDD[(String, String)] = sc.textFile("/Users/zhuji/tem_input")
      .map(str => {
        val tokens = str.replaceAll("[,.?\\-()\\d]", "").split(" ")
        val brod = neighborBroadcast.value
        for ((word, index) <- tokens.view.zipWithIndex;
             neighbor <- tokens.slice(if (index < brod) 0 else index - brod, if (index + brod > tokens.length) tokens.length - 1 else index + brod))
          yield (word, neighbor)
      })
      .flatMap(a => a)
      .persist()
    //    neighbors.saveAsTextFile("/Users/zhuji/neigh")
    //    val string = neighbors.toDebugString
    //    println(string)

    //(word,(word,toalcount))
    val wordTotalcount: RDD[(String, (String, Int))] = neighbors.groupByKey().mapValues(_.size).map(w => (w._1, w))
    //    wordTotalcount.saveAsTextFile("/Users/zhuji/word")

    //(word,(neighbor,count))
    val neighborCount: RDD[(String, (String, Int))] = neighbors.groupBy(t2 => t2).mapValues(_.size).map(wn => (wn._1._1, (wn._1._2, wn._2)))
    //    neighborCount.saveAsTextFile("/Users/zhuji/nei")

    import sparkSession.implicits._
    neighborCount.leftOuterJoin(wordTotalcount)
      .flatMap(join => join._2._2.map(wc => ((join._1, join._2._1._1), join._2._1._2 / wc._2.toDouble)))
      .toDF()
      .show(100)
  }
}
/*
+------------------+------------------+
|                _1|                _2|
+------------------+------------------+
|        [are, are]|             0.375|
|    [are, zhujing]|             0.125|
|        [are, who]|             0.125|
|        [are, you]|             0.375|
|        [you, are]|             0.375|
|        [you, who]|             0.125|
|     [you, dapang]|             0.125|
|        [you, you]|             0.375|
|     [dapang, you]|               0.5|
|     [dapang, are]|               0.5|
|     [am, zhujing]|0.3333333333333333|
|           [am, i]|0.3333333333333333|
|          [am, am]|0.3333333333333333|
|            [i, i]|               0.5|
|           [i, am]|               0.5|
|        [who, who]|              0.25|
|    [who, zhujing]|              0.25|
|         [who, am]|              0.25|
|        [who, are]|              0.25|
|[zhujing, zhujing]|              0.25|
|    [zhujing, who]|              0.25|
|      [zhujing, i]|              0.25|
|     [zhujing, am]|              0.25|
+------------------+------------------+
 */