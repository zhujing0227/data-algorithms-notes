package com.zhuji.secondarysort.spark

import org.apache.spark.SparkContext

/**
  * description: SecondarySort
  *
  */

object SecondarySort {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local", "secondarysort")
    val rdd = sc.textFile("/Users/zhuji/tem_input")
      .map(line => line.split(","))
      .map(x => ((x(0), x(1)), x(3)))
      .groupByKey()
      .sortByKey(ascending = false)
      .map(x => (x._1._1 + "-" + x._1._2, x._2.toList.sortWith(_ > _)))
    val valueRdd = rdd.map(x => x._1 + "\t" + x._2.mkString(",")).persist()
    valueRdd.saveAsTextFile("/Users/zhuji/temscala")
    sc.stop()
  }
}
