package com.zhuji.recommendationengine

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

/**
  * description: Cwbtiab
  * 购买过该商品的顾客还购买了哪些商品, 取 top3
  */

object Cwbtiab {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local", "MovingAverage")
    val sparkSession = SparkSession.builder().getOrCreate()

    //(userId,productIds)
    val products: RDD[(String, Iterable[String])] = sc.textFile("/Users/zhuji/tem_input")
      .map(s => {
        val tokens = s.split(",")
        (tokens(0), tokens(1))
      })
      .groupByKey()
      .cache()

    val value = products.map(_._2.toArray.sorted.toSet)
      .map(arr => {
        for ((p, index) <- arr.zipWithIndex;
             p1 <- arr.slice(index + 1, arr.size))
          yield (p, p1)
      })
      .flatMap(a => a)
      .aggregateByKey(Map[String, Int]())((m, p) => m.updated(p, m.getOrElse(p, 0) + 1), _ ++ _)
      .cache()

    import sparkSession.implicits._
    value.toDF("pid", "combine")
      .show(false)
    /*
    +---+------------------------------------+
    |pid|combine                             |
    +---+------------------------------------+
    |p1 |[p2 -> 1, p5 -> 2, p4 -> 1]         |
    |p2 |[p1 -> 1, p5 -> 1, p3 -> 1, p4 -> 1]|
    |p3 |[p2 -> 1, p1 -> 1, p4 -> 1, p5 -> 1]|
    |p5 |[p3 -> 1, p2 -> 1, p1 -> 1]         |
    |p4 |[p1 -> 1, p3 -> 1, p2 -> 1, p5 -> 2]|
    +---+------------------------------------+
     */

    //(pid,top3_pids)
    val top3: RDD[(String, Array[String])] = value.mapValues(m => m.toArray.sortBy(_._2).reverse.take(3).map(_._1))
      .cache()

    top3.toDF("pid", "top3")
      .show(false)
    /*
    +---+------------+
    |pid|top3        |
    +---+------------+
    |p1 |[p5, p4, p2]|
    |p2 |[p4, p3, p5]|
    |p3 |[p5, p4, p1]|
    |p5 |[p1, p2, p3]|
    |p4 |[p5, p2, p3]|
    +---+------------+
     */

  }
}

/*
#input
uid1,p1
uid1,p2
uid1,p3
uid1,p4
uid1,p5
uid2,p2
uid2,p3
uid2,p4
uid3,p1
uid3,p4
uid3,p5
uid4,p3
uid4,p3
uid4,p5
uid5,p1
uid5,p2
uid5,p5
 */