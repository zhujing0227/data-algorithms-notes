package com.zhuji.recommendationengine

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

/**
  * description: RecommendFriend
  * 好友推荐: 对于每个人P, 确定一个列表P1,P2,...,Pn, 这是与P 有最多共同好友的n个人
  */

object RecommendFriend {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local", "RecommendFriend")
    val sparkSession = SparkSession.builder().getOrCreate()

    //(person,Set[(possibleFriend,mutualFriend)])
    val possibleFriends: RDD[(Int, Set[(Int, Int)])] = sc.textFile("/Users/zhuji/tem_input")
      .map(l => {
        val arr = l.split(":")
        (arr(0), arr(1).split(",").sorted)
      })
      .map(p => {
        val directFriends = p._2.map(f => (p._1.toInt, (f.toInt, -1)))
        val possibleFriends =
          for ((f1, i) <- p._2.zipWithIndex;
               f2 <- p._2.slice(i, p._2.length))
            yield Array((f1.toInt, sortTuple2(f2.toInt, p._1.toInt)), (f2.toInt, sortTuple2(f1.toInt, p._1.toInt)))
        possibleFriends.flatten ++ directFriends
      })
      .flatMap(a => a)
      .groupByKey()
      .map(all => {
        val set = all._2.toSet
        val alreadyFriends = set.filter(a => a._2 == -1).map(_._1)
        (all._1,
          set.filterNot(a => a._2 == -1)
            .filterNot(a => alreadyFriends.contains(a._1))
            .filterNot(a => a._1 == all._1))
      })
      .cache()

    import sparkSession.implicits._
    possibleFriends.mapValues(_.mkString).toDF().show(false)
    /*
    +---+----------------------------------------+
    |_1 |_2                                      |
    +---+----------------------------------------+
    |4  |(7,1)(5,2)(5,1)(3,1)(8,1)(3,2)(7,2)     |
    |1  |                                        |
    |6  |(7,1)(5,1)(3,1)(8,1)(2,1)               |
    |3  |(7,1)(5,2)(5,1)(6,1)(4,1)(8,1)(4,2)(7,2)|
    |7  |(5,2)(5,1)(3,1)(6,1)(4,1)(8,1)(3,2)(4,2)|
    |8  |(7,1)(5,1)(3,1)(6,1)(4,1)(2,1)          |
    |5  |(7,1)(3,1)(6,1)(4,1)(8,1)(3,2)(4,2)(7,2)|
    |2  |(6,4)(6,1)(8,1)                         |
    +---+----------------------------------------+
     */

    //(person,Map[recommendFriend,mutualFriendStr])
    val result: RDD[(Int, Map[Int, String])] = possibleFriends.mapValues(iter => {
      iter.groupBy(_._1).mapValues(mf => {
        val list = mf.map(_._2).toList
        list.mkString(s"(${list.size}:[", ",", "])")
      })
        //MapLike SerializableException https://issues.scala-lang.org/browse/SI-7005,https://stackoverflow.com/questions/17709995/notserializableexception-for-mapstring-string-alias/17733446
        //mapValues(...).map(identity) will force conversion to a serializable instance. https://github.com/scala/bug/issues/7005
        .map(identity)
    })
      .sortBy(_._1)
      .cache()

    result.toDF("person", "recommend->(mutualCount:[mutualIds])")
      .show(false)
    /*
    +------+------------------------------------------------------------------------------------+
    |person|recommend->(mutualCount:[mutualIds])                                                |
    +------+------------------------------------------------------------------------------------+
    |1     |[]                                                                                  |
    |2     |[8 -> (1:[1]), 6 -> (2:[4,1])]                                                      |
    |3     |[5 -> (2:[2,1]), 6 -> (1:[1]), 7 -> (2:[1,2]), 8 -> (1:[1]), 4 -> (2:[1,2])]        |
    |4     |[8 -> (1:[1]), 5 -> (2:[2,1]), 7 -> (2:[1,2]), 3 -> (2:[1,2])]                      |
    |5     |[6 -> (1:[1]), 7 -> (2:[1,2]), 3 -> (2:[1,2]), 8 -> (1:[1]), 4 -> (2:[1,2])]        |
    |6     |[5 -> (1:[1]), 2 -> (1:[1]), 7 -> (1:[1]), 3 -> (1:[1]), 8 -> (1:[1])]              |
    |7     |[5 -> (2:[2,1]), 6 -> (1:[1]), 3 -> (2:[1,2]), 8 -> (1:[1]), 4 -> (2:[1,2])]        |
    |8     |[5 -> (1:[1]), 6 -> (1:[1]), 2 -> (1:[1]), 7 -> (1:[1]), 3 -> (1:[1]), 4 -> (1:[1])]|
    +------+------------------------------------------------------------------------------------+
     */
  }

  def sortTuple2(tuple2: (Int, Int)): (Int, Int) = if (tuple2._1 > tuple2._2) tuple2 else tuple2.swap
}

/*
#input
1:2,3,4,5,6,7,8
2:1,3,4,5,7
3:1,2
4:1,2,6
5:1,2
6:1,4
7:1,2
8:1
 */