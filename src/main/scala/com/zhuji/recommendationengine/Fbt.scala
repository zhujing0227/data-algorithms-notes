package com.zhuji.recommendationengine

import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession

/**
  * description: Fbt
  * 经常一起购买的商品
  */

object Fbt {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local", "MovingAverage")
    val sparkSession = SparkSession.builder().getOrCreate()

    val value = sc.textFile("/Users/zhuji/tem_input")
      .map(l => l.split(":")(1).split(",").sorted)
      .map(sorted => {
        for ((p, index) <- sorted.zipWithIndex;
             p1 <- sorted.slice(index + 1, sorted.length))
          yield (p, p1)
      })
      .flatMap(a => a)
      .groupByKey()
      .mapValues(_.toSet)
      .cache()

    import sparkSession.implicits._
    value.toDF("pid", "fbt")
      .show(false)
    /*
    +---+--------+
    |pid|fbt     |
    +---+--------+
    |p1 |[p2, p3]|
    |p2 |[p3, p4]|
    |p3 |[p4]    |
    |p5 |[p6]    |
    +---+--------+
     */

  }
}

/*
#input
t1:p1,p2,p3
t2:p2,p3
t3:p2,p3,p4
t4:p5,p6
t5:p6
t6:p5
 */