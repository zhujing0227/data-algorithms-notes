name := "data-algorithms-notes"

version := "0.1"

scalaVersion := "2.11.12"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-sql" % "2.3.1",
  "org.apache.spark" %% "spark-hive" % "2.3.1" % "provided",
  "org.apache.spark" %% "spark-mllib" % "2.3.2",
  "mysql" % "mysql-connector-java" % "5.1.41",
  "org.springframework.boot" % "spring-boot-starter" % "2.0.5.RELEASE",
  "org.springframework.boot" % "spring-boot-starter-data-jpa" % "2.0.5.RELEASE",
  "org.apache.hadoop" % "hadoop-common" % "3.1.1",
  "org.apache.hadoop" % "hadoop-client" % "3.1.1",
  "org.apache.hadoop" % "hadoop-hdfs" % "3.1.1",
  "org.projectlombok" % "lombok" % "1.18.2" % "provided",
  "com.alibaba" % "fastjson" % "1.2.51"
)

dependencyOverrides ++= Seq(
  "com.fasterxml.jackson.core" % "jackson-core" % "2.8.7",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.8.7",
  "com.fasterxml.jackson.module" % "jackson-module-scala_2.11" % "2.8.7"
)


excludeDependencies ++= Seq(
  ExclusionRule("ch.qos.logback", "logback-classic")
)